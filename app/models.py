from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), unique=True)
    cpf = models.CharField(max_length=11, unique=True, blank=True)
    date_of_birth = models.DateField(blank=True)
    phone = models.CharField(max_length=11, blank=True)
    address = models.OneToOneField('Address', on_delete=models.SET_NULL, null=True)
    avatar = models.ImageField(upload_to='images/avatars', null=True, blank=True)
    date_joined = models.DateField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=False)
    is_staff = models.BooleanField(_('staff status'), default=False)
    created = models.DateTimeField(default=timezone.now, editable=False)

    object = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name =_('user')
        verbose_name_plural =_('users')

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        :param subject:
        :param message:
        :param from_email:
        :param kwargs:
        :return:
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)


class EcoFriend(models.Model):
    user = models.OneToOneField(User, related_name='eco_friend', on_delete=models.CASCADE)
    created = models.DateTimeField(default=timezone.now, editable=False)

    class Meta:
        db_table = 'app_eco_friend'


class EcoRecycler(models.Model):
    user = models.OneToOneField(User, related_name='eco_recycle', on_delete=models.CASCADE)
    declaration = models.ImageField(upload_to='images/declaration', null=False)
    document = models.ImageField(upload_to='images/document', null=False)
    created = models.DateTimeField(default=timezone.now, editable=False)

    class Meta:
        db_table = 'app_eco_recycler'


class Address(models.Model):
    logradouro = models.CharField(max_length=80, null=False)
    description = models.CharField(max_length=100, null=True)
    number = models.CharField(max_length=50, null=False)
    sector = models.CharField(max_length=50, null=False)
    city = models.CharField(max_length=100, null=False)
    uf = models.CharField(max_length=2, null=False)
    cep = models.CharField(max_length=9, null=False)
    created = models.DateTimeField(default=timezone.now, editable=False)


class City(models.Model):
    name = models.CharField(max_length=20, null=False)
    uf = models.CharField(max_length=2, null=False)


class Location(models.Model):
    latitude = models.CharField(max_length=50, blank=False, null=False)
    longitude = models.CharField(max_length=50, blank=False, null=False)


class Evalution(models.Model):
    grade = models.PositiveSmallIntegerField(validators=[MaxValueValidator(5)], blank=False, null=False)
    comment = models.TextField()
    created = models.DateTimeField(default=timezone.now, editable=False)


class TypeOfMaterial(models.Model):
    type = models.CharField(max_length=100, unique=True, null=False)
    description = models.TextField(null=False)

    class Meta:
        db_table = 'app_type_of_material'

    def __str__(self):
        return self.type



class Request(models.Model):
    STATUS_CHOICES = (
        ('AB', 'Aberto'),
        ('PE', 'Pendente'),
        ('FI', 'Finalizado'),
        ('CA', 'Cancelado')
    )
    AVAILABLE_CHOICES = (
        ('MA', 'Manhã'),
        ('TA', 'Tarde'),
        ('NO', 'Noite')
    )

    eco_friend = models.ForeignKey(EcoFriend, on_delete=models.PROTECT)
    eco_recycler = models.PositiveIntegerField(null=True)
    status = models.CharField(max_length=10, default='Aberto', choices=STATUS_CHOICES)
    location = models.ForeignKey(Location, on_delete=None)
    address = models.ForeignKey(Address, on_delete=models.PROTECT)
    available = models.CharField(max_length=20, null=True, choices=AVAILABLE_CHOICES)
    image = models.ImageField(upload_to="images/collections", null=True, blank=True)
    created = models.DateTimeField(default=timezone.now, editable=False)


class Collect(models.Model):
    request = models.ForeignKey(Request, related_name='collects', on_delete=models.CASCADE)
    type = models.CharField(max_length=50, null=False)
    created = models.DateTimeField(default=timezone.now, editable=False)


class CollectionPoint(models.Model):
    location = models.OneToOneField(Location, related_name='collection_point', on_delete=models.CASCADE)
    name = models.CharField(max_length=100, null=False)
    description = models.TextField(null=True)

    class Meta:
        db_table = 'app_collection_point'