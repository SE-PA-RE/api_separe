from django.contrib import admin
from .models import *
# Register your models here.
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'email', 'cpf', 'is_active', 'is_staff']


@admin.register(TypeOfMaterial)
class TypeOfMaterialAdmin(admin.ModelAdmin):
    list_display = ['type', 'description']


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ['name', 'uf']

