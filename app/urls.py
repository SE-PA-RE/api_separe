from django.conf.urls import url
from django.urls import include
from rest_framework.routers import SimpleRouter
import oauth2_provider.views as oauth2_views
from app.views import *

# OAuth2 provider endpoints
oauth2_endpoint_views = [
    url(r'^token/$', oauth2_views.TokenView.as_view(), name="token")
]

# Url de ativação de conta do EcoFriend
urls = [

]

router = SimpleRouter()
router.register(r'users', UserViewSet, basename='user')
router.register(r'friends', EcoFriendViewSet, basename='friend')
router.register(r'recyclers', EcoRecyclerViewSet, basename='recycler')
router.register(r'types', TypeOfMaterialViewSet, basename='type')
router.register(r'collections', CollectionViewSet, basename='collection')
router.register(r'cities', CitySetView, basename='city')
router.register(r'points', CollectionPointSetView, basename='points')

urlpatterns = router.urls + oauth2_endpoint_views + urls