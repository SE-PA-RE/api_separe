from django.contrib.sites.shortcuts import get_current_site
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404


# Confirmação de email
from django.template.loader import render_to_string
from django.utils.encoding import force_text, force_bytes
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from oauth2_provider.contrib.rest_framework import OAuth2Authentication
from rest_framework.decorators import action

from app.utils import send_email_activated

from api_separe import settings
from app.tokens import account_activation_token

# Create your views here.
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from .serializers import *
from .permissions import *
from .pagination import *


class UserViewSet(viewsets.ViewSet):
    queryset = User.object.all()
    serializer_class = UserSerializer
    authentication_classes = [OAuth2Authentication]

    def get_permissions(self):
        if self.action == 'list':
            permission_classes = [IsAuthenticated, IsAdminUser]
        else:
            permission_classes = []
        return [permission() for permission in permission_classes]

    def list(self, request):
        """
        Lista todos os usuários cadastrado
        :param request:
        :return:
        """
        try:
            serializer = self.serializer_class(self.queryset, many=True)
            return Response({'return': 'OK',
                             'data': serializer.data},
                              status=status.HTTP_200_OK)
        except Exception:
            return Response({'return': 'NOK',
                             'message': 'Ocorreu um error no servidor'},
                              status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(detail=False, methods=['post'])
    def reset_password(self, request):
        try:
            if not request.data.get('email'):
                return Response({'return': 'NOK',
                                 'message': 'O email precisa ser definido'},
                                  status=status.HTTP_400_BAD_REQUEST)
            user = self.queryset.get(email=request.data.get('email'))
            password = User.object.make_random_password()
            user.set_password(password)
            user.save()
            mail_subject = 'Nova senha'
            message = render_to_string('reset_password.html', {
                'user': user,
                'password': password
            })
            send_email_activated._send_email(mail_subject, message, settings.EMAIL_HOST, user.email)
            return Response({'return': 'OK',
                             'message': 'Sua senha foi redefinida com sucesso. Por favor, verifique seu e-mail para obter instruções sobre como escolher uma nova senha.'})
        except User.DoesNotExist:
            return Response({'return': 'NOK',
                                 'message': 'Não foi encontrado cadastro com email informado.'},
                                 status=status.HTTP_204_NO_CONTENT)
        except Exception:
            return Response({'return': 'NOK',
                             'message': 'Ocorreu um error no servidor.'},
                              status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(detail=False, methods=['get'], permission_classes=[IsAuthenticated])
    def get_profile(self, request):
        try:
            serializer = self.serializer_class(request.user)
            return Response({'return': 'OK',
                             'message': 'Perfil do usuário',
                             'data': serializer.data})
        except Exception:
            return Response({'return': 'NOK',
                             'message': 'Ocorreu um error no servidor.'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(detail=False, methods=['get'], permission_classes=[IsAuthenticated])
    def get_address(self, request):
        try:
            serializer = self.serializer_class(request.user)
            if serializer.data['address']:
                return Response({'return': 'OK',
                                 'message': 'Endereço do Usuário',
                                 'data': serializer.data['address']})
            else:
                return Response({'return': 'OK',
                                 'message': 'Usuário não possui endereço cadastrado'}, status=status.HTTP_404_NOT_FOUND)
        except Exception:
            return Response({'return': 'NOK',
                             'message': 'Ocorreu um error no servidor.'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class EcoFriendViewSet(viewsets.ViewSet):
    queryset = EcoFriend.objects.all()
    serializer = EcoFriendSerializer
    authentication_classes = [OAuth2Authentication]

    def create(self, request):
        try:
            user_serializer = UserSerializer(data=request.data)
            if user_serializer.is_valid():
                user = user_serializer.save()
                current_site = get_current_site(request)
                mail_subject = 'Ative sua Conta.'
                message = render_to_string('active_email.html', {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                })
                send_email_activated._send_email(mail_subject, message, settings.EMAIL_HOST, user.email)
                return Response({'return': 'OK',
                                 'message': 'Seu cadastro foi realizado com sucesso, acesse seu email para ativar sua conta'},
                                  status=status.HTTP_201_CREATED)
            return Response({'return': 'NOK',
                             'message': 'Não foi possível processar os dados',
                             'data': user_serializer.errors},
                              status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except Exception:
            return Response({'return': 'NOK',
                             'message': 'Ocorreu um error no servidor.'},
                              status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class EcoRecyclerViewSet(viewsets.ViewSet):
    queryset = EcoRecycler.objects.all()
    serializer_class = EcoRecyclerSerializer
    authentication_classes = [OAuth2Authentication]

    def create(self, request):
        try:
            serializer = self.serializer_class(data={'user': request.data,
                                                     'document': request.data.get('document'),
                                                     'declaration': request.data.get('declaration')})
            if serializer.is_valid():
                serializer.save()
                return Response({'return': 'OK',
                                 'message': 'Seu cadastro foi realizado com sucesso, sua documentação será analisada. Fique atento que você receberá o email para ativar sua conta'},
                                status=status.HTTP_201_CREATED)
            return Response({'return': 'OK',
                             'message': 'Não foi possível processar os dados',
                             'data': serializer.errors},
                            status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except Exception:
            return Response({'return': 'NOK',
                             'message': 'Ocorreu um error no servidor'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class TypeOfMaterialViewSet(viewsets.ViewSet):
    queryset = TypeOfMaterial.objects.all()
    serializer_class = TypeOfMaterialSerializer
    authentication_classes = [OAuth2Authentication]

    def get_permissions(self):
        if self.action == 'create':
            permission_classes = [IsAdminUser]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    def list(self, request):
        try:
            serializer = self.serializer_class(self.queryset, many=True)
            return Response({'return': 'OK',
                             'message': 'Lista de Tipos de materiais',
                             'data': serializer.data})
        except Exception:
            return Response({'return': 'NOK',
                             'message': 'Ocorreu um error no servidor'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def create(self, request):
        try:
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'return': 'OK',
                                'message': 'Tipo de coleta cadastrado com sucesso'},
                                status=status.HTTP_201_CREATED)
            return Response({'return': 'NOK',
                             'message': 'Não foi possível processar os dados',
                             'data': serializer.errors},
                             status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except Exception:
            return Response({'return': 'NOK',
                             'message': 'Ocorreu um error no servidor'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CollectionViewSet(viewsets.ViewSet):
    queryset = Request.objects.all()
    serializer_class = RequestSerializer
    authentication_classes = [OAuth2Authentication]

    def get_permissions(self):
        if self.action == 'create':
            permission_classes = [IsEcoFriend]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    def list(self, request):
        try:
            paginator = RequestPagination()
            result_page = paginator.paginate_queryset(self.queryset, request)
            serializer = self.serializer_class(result_page, many=True)
            return paginator.get_paginated_response(serializer.data)
        except Exception:
            return Response({'return': 'NOK',
                             'message': 'Ocorreu um error no servidor'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def create(self, request):
        try:
            request.data.setdefault('eco_friend', request.user.pk)
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'return': 'OK',
                                'message': 'Sua coleta foi cadastrada com sucesso'},
                                status=status.HTTP_201_CREATED)
            return Response({'return': 'NOK',
                            'message': 'Não foi possível processar os dados',
                            'data': serializer.errors},
                            status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except Exception:
            return Response({'return': 'NOK',
                             'message': 'Ocorreu um error no servidor'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def retrieve(self, request, pk=None):
        collection = get_object_or_404(self.queryset, pk=pk)
        serializer = self.serializer_class(collection)
        return Response({'return': 'OK',
                        'message': '',
                        'data': serializer.data})
        # return Response({'return': 'NOK',
        #                 'message': 'Pedido de coleta não encotrado com ID informado'},
        #                 status=status.HTTP_204_NO_CONTENT)
        

class CitySetView(viewsets.ViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    permission_classes = [IsAuthenticated]

    def list(self, request):
        # try:
        serializer = self.serializer_class(self.queryset, many=True)
        return Response({'return': 'OK',
                         'message': 'Lista de cidades',
                         'data': serializer.data})
        # except Exception:
        #     return Response({'return': 'NOK',
        #                      'message': 'Ocorreu um error no servidor'},
        #                     status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    # def create(self, request):


class CollectionPointSetView(viewsets.ViewSet):
    queryset = CollectionPoint.objects.all()
    serializer_class = CollectionPointSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request):
        try:
            serializer = self.serializer_class(self.queryset, many=True)
            return Response({'return': 'OK',
                             'message': 'Lista de pontos',
                             'data': serializer.data})
        except Exception:
            return Response({'return': 'NOK',
                             'message': 'Ocorreu um error no servidor'},
                             status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def activate(request, uidb64, token, template_name='activate.html'):
    """
    Valida email e ativa a conta do Eco Friend
    :param uidb64:
    :param token:
    :param template_name:
    :return:
    """
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        print(uid)
        user = User.object.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return render(request, template_name, {'name': user.first_name})
    return render(request, template_name)
