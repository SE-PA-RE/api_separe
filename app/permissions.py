from rest_framework import permissions


class IsEcoFriend(permissions.BasePermission):

    def has_permission(self, request, view):
        return bool(request.user and hasattr(request.user, 'eco_friend'))