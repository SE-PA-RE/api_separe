import datetime

from rest_framework import serializers
from .utils import Utils
from .exception import ExceptionMessage


def cpf_validator(value):
    """
    Verifica se o cpf é válido
    :param value:
    :return:
    """
    if len(value) != 11 or len(set(value)) == 1:
        raise serializers.ValidationError(ExceptionMessage().invalid_cpf())

    first_cpf_weight = [10, 9, 8, 7, 6, 5, 4, 3, 2]
    second_cpf_weight = [11, 10, 9, 8, 7, 6, 5, 4, 3, 2]
    first_part = value[:9]
    first_digit = value[9]
    second_digit = value[10]

    if not ((first_digit == Utils().get_first_digit(number=first_part, weight=first_cpf_weight)
             and second_digit == Utils().get_second_digit(updated_number=value[:10], weight=second_cpf_weight))):
        raise serializers.ValidationError(ExceptionMessage().invalid_cpf())


def greater_age_validator(value):
    """
    Valida se a idade é maior que 18 anos
    :param value:
    :return:
    """
    today_date = datetime.datetime.now()
    max_date = datetime.date(today_date.year - 18, today_date.month, today_date.day)
    if value > max_date:
        raise serializers.ValidationError(ExceptionMessage().invalid_date_of_birth())