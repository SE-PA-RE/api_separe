from threading import Thread

from django.core.mail import EmailMessage


class Utils():
    def __init__(self):
        self.divider = 11

    def get_first_digit(self, number, weight):
        sum = 0

        for i in range(len(weight)):
            sum += int(number[i]) * weight[i]

        rest_division = sum % self.divider

        if rest_division < 2:
            return '0'

        return str(11 - rest_division)

    def get_second_digit(self, updated_number, weight):
        sum = 0

        for i in range(len(weight)):
            sum += + int(updated_number[i]) * weight[i]

        rest_division = sum % self.divider

        if rest_division < 2:
            return '0'

        return str(11 - rest_division)


class Tools():

    def start_new_thread(function):
        def decorator(*args, **kwargs):
            t = Thread(target=function, args=args, kwargs=kwargs)
            t.daemon = True
            t.start()

        return decorator

    @start_new_thread
    def _send_email(self, mail_object, message, from_email, to_email ):
        msg = EmailMessage(mail_object,message, from_email, [to_email])
        msg.send()


send_email_activated = Tools()