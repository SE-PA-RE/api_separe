from django.conf import settings


class ExceptionMessage():
    def __init__(self):
        self.LANGUAGE_CODE = settings.LANGUAGE_CODE

        self.cpf = [
            {'lang': 'en-us', 'message-cpf': 'Invalid CPF', 'message-cnpj': 'Invalid CNPJ'},
            {'lang': 'pt-br', 'message-cpf': 'CPF inválido', 'message-cnpj': 'CNPJ inválido'},
            {'lang': 'es-es', 'message-cpf': 'CPF no válido', 'message-cnpj': 'CNPJ no válido'},
        ]
        self.date_of_birth = [
            {'lang': 'en-us', 'message-date': 'User can not be less than 18 years old'},
            {'lang': 'pt-br', 'message-date': 'Idade não pode ser menor de 18 anos'},
            {'lang': 'es-es', 'message-date': 'El usuario no puede ser menor de 18 años'},
        ]

    def invalid_cpf(self):
        for tr in self.cpf:
            if tr['lang'] == self.LANGUAGE_CODE:
                return tr['message-cpf']

    def invalid_cnpj(self):
        for tr in self.cpf:
            if tr['lang'] == self.LANGUAGE_CODE:
                return tr['message-cnpj']

    def invalid_date_of_birth(self):
        for tr in self.date_of_birth:
            if tr['lang'] == self.LANGUAGE_CODE:
                return tr['message-date']