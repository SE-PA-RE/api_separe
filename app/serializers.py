import datetime

from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from app.validators import cpf_validator, greater_age_validator
from .models import *
from drf_extra_fields.fields import Base64ImageField

from drf_writable_nested import WritableNestedModelSerializer


class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    password = serializers.CharField(write_only=True, required=True)
    cpf = serializers.CharField(validators=[cpf_validator, UniqueValidator(queryset=User.object.all(), message='CPF já em uso.')])
    date_of_birth = serializers.DateField(validators=[greater_age_validator])
    address = AddressSerializer(required=False)

    class Meta:
        model = User
        fields = ('id', 'email', 'password', 'first_name', 'last_name', 'cpf', 'date_of_birth', 'address', 'eco_friend', 'eco_recycle')
        read_only_fields = ('eco_friend', 'eco_recycle')
        validators = []
        
    def create(self, validated_data):
        user = User.object.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class EcoFriendSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)

    class Meta:
        model = EcoFriend
        fields = ('id', 'user')

    def create(self, validated_data):
        user = User.object.create(**validated_data)
        EcoFriend.object.create(user_id=user.pk)
        return user


class EcoRecyclerSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)
    declaration = Base64ImageField(required=True)
    document = Base64ImageField(required=True)

    class Meta:
        model = EcoRecycler
        fields = ('id', 'user', 'declaration', 'document')

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user = User.object.create(**user_data)
        user.set_password(user_data['password'])
        user.save()
        eco_recycler = EcoRecycler.objects.create(user=user, **validated_data)
        return eco_recycler


class TypeOfMaterialSerializer(serializers.ModelSerializer):

    class Meta:
        model = TypeOfMaterial
        fields = ('id', 'type', 'description')


class LocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Location
        fields = '__all__'


class CollectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Collect
        fields = ('id', 'type')


class RequestSerializer(WritableNestedModelSerializer):
    collects = CollectSerializer(required=True, many=True)
    location = LocationSerializer()
    address = AddressSerializer(required=True)
    image =  Base64ImageField(required=False)

    class Meta:
        model = Request
        fields = ('id', 'eco_friend', 'eco_recycler', 'status', 'location', 'address', 'available', 'image', 'collects')


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = '__all__'


class CollectionPointSerializer(serializers.ModelSerializer):
    location = LocationSerializer()

    class Meta:
        model = CollectionPoint
        fields = ('id', 'name', 'description', 'location')