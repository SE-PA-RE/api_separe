
INSERT INTO `oauth2_provider_application` VALUES (1,'olmLLAzbPRd7OrWyRdGl9in2so7MQySWMHO7R5f5','http://localhost','confidential','password','IQz984Jyy5KdmoM9EfiOq0VuSph9EIbK7pebIj20H3Vx2yptUB6bUWFUP3uBbSaS7Rng5yV6o5UWwqYl8lD1Ny8v0oEOylSGQDFS9cVDer6D3Bl4gEsua05nzLqJZWh9','app',NULL,0,'2019-05-16 03:24:15.156222','2019-05-16 03:24:15.156465');

INSERT INTO `app_user` VALUES (1,'pbkdf2_sha256$150000$TY8lkLVMdlHq$lbG49vPMzTyO9umqt4qjbSKvEoO/HmqW81nF6sASUxc=','2019-05-16 03:23:41.693814',1,'wyllian','sales','wylliansalles@gmail.com','02240436174','2001-05-15','','','2019-05-16',1,1,'2019-05-16 03:21:17.742388',NULL);

INSERT INTO `app_eco_friend` VALUES (1,'2019-05-16 03:21:18.050918',1);

INSERT INTO `app_location` (id,latitude,longitude) VALUES (1,'-10.171058654785156','-48.33458709716797');
INSERT INTO `app_location` (id,latitude,longitude) VALUES (2,'-10.177135467529297','-48.328948974609375');
INSERT INTO `app_location` (id,latitude,longitude) VALUES (3,'-10.209824562072754','-48.323246002197266');
INSERT INTO `app_location` (id,latitude,longitude) VALUES (4,'-10.20461654663086','-48.338600158691406');

INSERT INTO `app_collection_point` (id,name,description,location_id) VALUES (1,'EcoPonto 01','Escola Padre Josino Tavares - Região Norte',1);
INSERT INTO `app_collection_point` (id,name,description,location_id) VALUES (2,'EcoPonto 02','Parque dos Povos Indígenas - Região Norte',2);
INSERT INTO `app_collection_point` (id,name,description,location_id) VALUES (3,'EcoPonto 03','Parque Cesamar - Região Sul',3);
INSERT INTO `app_collection_point` (id,name,description,location_id) VALUES (4,'EcoPonto 04','Parque dos Idosos de Palmas - Região Sul',4);

INSERT INTO `app_type_of_material` (id,type,description) VALUES (1,'Plástico','Os plásticos são materiais orgânicos poliméricos sintéticos, de constituição macromolecular, dotada de grande maleabilidade (que apresentam a propriedade de adaptar-se em distintas formas), facilmente transformável mediante o emprego de calor e pressão, e que serve de matéria-prima para a fabricação dos mais variados objetos: vasos, sacola, toalhas, embalagens, cortinas, bijuterias, carrocerias, roupas, sapatos e etc.');
INSERT INTO `app_type_of_material` (id,type,description) VALUES (2,'Papel','....');
INSERT INTO `app_type_of_material` (id,type,description) VALUES (3,'Vidro','....');

COMMIT;
